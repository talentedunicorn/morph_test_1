import { createRouter, createWebHistory } from 'vue-router'
import PostsLayout from '@/layouts/PostsLayout.vue'
import AuthLayout from '@/layouts/AuthLayout.vue'
import HomeView from '@/views/HomeView.vue'
import LoginView from '@/views/LoginView.vue'
import RegisterView from '@/views/RegisterView.vue'
import PostsView from '@/views/PostsView.vue'
import NewPostView from '@/views/NewPostView.vue'
import NotFoundView from '@/views/NotFoundView.vue'
import { LoginGuard } from '@/guards/login.guard'
import { AuthGuard } from '@/guards/auth.guard'
import EditPostView from '@/views/EditPostView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/auth',
      component: AuthLayout,
      beforeEnter: LoginGuard,
      children: [
        {
          path: '',
          name: 'login',
          component: LoginView
        },
        {
          path: 'register',
          name: 'register',
          component: RegisterView
        }
      ]
    },
    {
      path: '/posts',
      beforeEnter: AuthGuard,
      component: PostsLayout,
      children: [
        {
          path: '',
          name: 'posts',
          component: PostsView
        },
        {
          path: 'new',
          name: 'newPost',
          component: NewPostView
        },
        {
          path: 'edit/:id',
          name: 'editPost',
          component: EditPostView
        }
      ]
    },
    {
      path: '/:pathMatch(.*)*',
      name: 'notfound',
      component: NotFoundView
    }
  ]
})

export default router

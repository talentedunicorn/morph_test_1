import { AUTH_KEY } from '@/constants'

export interface Token {
  id: string
  name: string
  username: string
  sessionId: string
}

export const saveToken = (token: Token) => localStorage.setItem(AUTH_KEY, JSON.stringify(token))

export const getToken = (): null | Token =>
  localStorage.getItem(AUTH_KEY) ? JSON.parse(localStorage.getItem(AUTH_KEY)!) : null

export const isAuthed = (): boolean => getToken() !== null

export const clearToken = () => localStorage.removeItem(AUTH_KEY)

# morphtest_1

> Full stack technical test

## Requirements

- Create a website consist of a login page, registration page, and a content page with navigation
- A MySQL database that store registration and manage page contents
- You can demonstrate your front-end and back-end skills in content pages, such as:
   - (must) Async function using promise method
   - (must) Advance DB query demonstration
   - (must) AJAX content loading in modal
   - (optional) editable table listing
   - (optional) carousel content card
   -  Etc. (any other special feature you would like to show)
- Git repo that we can download the project
- Step by step guide for us to deploy the project to our localhost and run

## Project Setup

### Enviroment variables
Create a `.env` file on the root of project. Then fill in the following variables

- `DATABASE_URL` : DB connection string (PostgreSQL)

```sh
pnpm install
```

### Setup database

```sh
pnpm prisma migrate dev
```


### Compile and Hot-Reload for Development (runs backend and frontend)

```sh
pnpm dev
```

### Type-Check, Compile and Minify for Production

```sh
pnpm build
```

### Lint with [ESLint](https://eslint.org/)

```sh
pnpm lint
```

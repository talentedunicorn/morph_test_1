import { isAuthed } from '@/lib/auth'
import type { NavigationGuard } from 'vue-router'

export const LoginGuard: NavigationGuard = (to, _, next) => {
  if ((to.name === 'login' || to.name === 'register') && isAuthed()) {
    next('/')
    return
  }
  next()
}

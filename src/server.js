import express from 'express'
import bodyParser from 'body-parser'
import { PrismaClient } from '@prisma/client'
import bcrypt from 'bcrypt'

const client = new PrismaClient()

const saveUser = (data) => {
  return client.user
    .create({
      data
    })
    .finally(() => {
      client.$disconnect()
    })
}

const validateUser = async (username) => {
  return client.user
    .findFirstOrThrow({
      where: {
        username
      }
    })
    .finally(() => {
      client.$disconnect()
    })
}

const createSession = async (userId) =>
  client.session.create({ data: { userId } }).finally(() => client.$disconnect())

const invalidateSession = async (sessionId) =>
  client.session
    .update({ where: { id: sessionId }, data: { active: false } })
    .finally(() => client.$disconnect())

// const isActiveSession = async (sessionId) => client.session.findFirst({ where: { id: sessionId, active: true }}) // Check if session is active

const getPosts = async () =>
  client.post
    .findMany({
      include: {
        author: {
          select: {
            id: true,
            name: true
          }
        }
      }
    })
    .finally(() => client.$disconnect())

const getPostById = (id) =>
  client.post.findFirstOrThrow({ where: { id } }).finally(() => client.$disconnect())

const createPost = async (data) => client.post.create({ data }).finally(() => client.$disconnect())

const updatePost = async (id, data) =>
  client.post
    .update({
      where: {
        id: id
      },
      data
    })
    .finally(() => client.$disconnect())

const deletePost = async (id) =>
  client.post.delete({ where: { id } }).finally(() => client.$disconnect())

const PORT = 3000
const app = express()

app.use(bodyParser.json())

app.get('/', (_, res) => {
  return res.send('Welcome to the API')
})

app.post(`/users`, async (req, res) => {
  if (!req.body) return res.status(400).send('Invalid input')
  try {
    // Hash password before saving
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await bcrypt.hash(req.body?.password, salt)
    const data = { ...req.body, password: hashedPassword }
    const user = await saveUser(data)

    return res.status(200).send(JSON.stringify(user))
  } catch (e) {
    return res.status(500).send(`Unable to create user, ${e}`)
  }
})

app.post('/login', async (req, res) => {
  const creds = req.body
  if (!creds || !creds?.username || !creds?.password) return res.status(400).send('Invalid input')
  try {
    const user = await validateUser(creds.username)
    const userWithoutPassword = Object.fromEntries(
      Object.entries(user).filter(([k]) => k !== 'password')
    )
    const isValid = await bcrypt.compare(creds.password, user.password)
    if (!isValid) return res.status(401).send()
    // Generate user session
    const session = await createSession(user.id)
    return res.status(200).send({ ...userWithoutPassword, sessionId: session.id })
  } catch (e) {
    return res.status(401).send()
  }
})

app.post(`/logout`, async (req, res) => {
  const { body } = req
  if (!body.token || body.token.length === 0) return res.status(400).send()
  try {
    await invalidateSession(body.token)
    return res.status(201).send()
  } catch (e) {
    return res.status(400).send(e)
  }
})

app.get(`/posts`, async (_, res) => {
  try {
    const posts = await getPosts()
    return res.send(posts)
  } catch (e) {
    return res.status(500).send(e.message)
  }
})

app.get(`/posts/:id`, async (req, res) => {
  const id = req.params.id
  if (!id) return res.send(400)
  try {
    const post = await getPostById(id)
    return res.send(post)
  } catch (e) {
    return res.send(404, e)
  }
})

app.post(`/posts`, async (req, res) => {
  const payload = req.body
  if (!payload || !payload.title || !payload.userId || payload.title.length < 3)
    return res.status(400).send()

  try {
    const post = await createPost(payload)
    return res.status(201).send(post)
  } catch (e) {
    return res.status(500).send(e)
  }
})

app.patch(`/posts/:id`, async (req, res) => {
  const id = req.params.id
  const payload = req.body

  if (!payload || !id || !payload.title || id.length === 0) return res.send(400)

  try {
    const post = await updatePost(id, payload)
    return res.status(201).send(post)
  } catch (e) {
    return res.send(e)
  }
})

app.delete(`/posts/:id`, async (req, res) => {
  const id = req.params.id

  if (id.length === 0) return res.send(400)

  try {
    const post = await deletePost(id)
    return res.status(201).send(post)
  } catch (e) {
    return res.send(e)
  }
})

app.listen(PORT, () => console.log(`Server started at http://localhost:${PORT}`))

import { isAuthed } from '@/lib/auth'
import type { NavigationGuard } from 'vue-router'

export const AuthGuard: NavigationGuard = (to, _, next) => {
  if (to.path.startsWith('/posts') && !isAuthed()) {
    next({ name: 'login' })
    return
  }
  next()
}
